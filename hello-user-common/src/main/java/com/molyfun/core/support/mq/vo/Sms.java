package com.molyfun.core.support.mq.vo;

import java.io.Serializable;


@SuppressWarnings("serial")
public class Sms implements Serializable{

	private String phone;
	private String content;


	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Sms(String phone, String content) {
		super();
		this.phone = phone;
		this.content = content;
	}



}
