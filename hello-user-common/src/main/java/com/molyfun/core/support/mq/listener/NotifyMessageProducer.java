package com.molyfun.core.support.mq.listener;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.jms.Destination;
import javax.jms.JMSException;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import javax.jms.Session;
import com.molyfun.core.support.mq.vo.Sms;
import javax.jms.Message;

/**
 * JMS用户变更消息生产者.
 * 
 * 使用jmsTemplate将用户变更消息分别发送到queue与topic.
 * 
 * @author calvin
 */

public class NotifyMessageProducer {

	private JmsTemplate jmsTemplate;
	private Destination notifyQueue;
	private Destination notifyTopic;

	public void sendQueue(final Sms sms) {
		sendMessage(sms, notifyQueue);
	}

	//	public void sendQueueDownPhoto(final DownPhotoObj obj) {
	//		sendMessageDownPhoto(obj, notifyQueue);
	//	}

	public void sendTopic(final Sms sms) {
		sendMessage(sms, notifyTopic);
	}


	public void send(String topicName, final Serializable message) {
		jmsTemplate.send(topicName, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				return session.createObjectMessage(message);
			}
		});
	}


	/**
	 * 使用jmsTemplate最简便的封装convertAndSend()发送Map类型的消息.
	 */
	private void sendMessage(Sms sms, Destination destination) {
		Map map = new HashMap();
		map.put("phone", sms.getPhone());
		map.put("content", sms.getContent());
		jmsTemplate.convertAndSend(destination, map);
	}

	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void setNotifyQueue(Destination notifyQueue) {
		this.notifyQueue = notifyQueue;
	}

	public void setNotifyTopic(Destination nodifyTopic) {
		this.notifyTopic = nodifyTopic;
	}
}
