package com.molyfun.web.user;


import com.alibaba.dubbo.common.utils.StringUtils;
import com.molyfun.core.Constants;
import com.molyfun.core.annotation.Authorization;
import com.molyfun.core.annotation.CurrentUser;
import com.molyfun.core.annotation.CurrentUserBean;
import com.molyfun.core.base.BaseController;
import com.molyfun.core.support.Assert;
import com.molyfun.core.support.HoUserService;
import com.molyfun.core.support.HttpCode;
import com.molyfun.core.util.RedisUtil;
import com.molyfun.core.util.SecurityUtil;
import com.molyfun.model.student.HoUserBean;
import com.molyfun.model.user.HoUser;
import com.molyfun.service.user.HoUserServiceImpl;
import com.molyfun.web.base.UserThreadLocal;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 *  用户登录
 *  shidongsheng
 *  2016.7.13
 */
@RequestMapping("/api/v1/user")
@RestController
@Api(description = "用户接口")
public class UserController extends BaseController {

	@Autowired
	public HoUserServiceImpl userService;
	
	// 验证码
	@ApiOperation(value = "验证码")
	@GetMapping("/captcha/{phone}")
	public Object captcha (@ApiParam(required = true, value = "需要参数:手机号") @PathVariable("phone") String phone) {
		Assert.isNotBlank(phone, "手机号码为空");
		String code = userService.captchaCode(phone);
		return setSuccessModelMap(code);
	}
	
	
	// 验证码
	@ApiOperation(value = "检查验证码")
	@GetMapping("/checkcaptcha/{phone}/{code}")
	public Object checkcaptcha (@ApiParam(required = true, value = "需要参数:验证码") @PathVariable("code") String code,
			@ApiParam(required = true, value = "需要参数:手机号") @PathVariable("phone") String phone) {
		Assert.isNotBlank(code, "验证码为空");
		boolean flag  = userService.checkCaptcha(phone,code);
		if(flag)
		{
			return setSuccessModelMap("");
		}
		return setModelMap(HttpCode.BUSINESS_FAIL,"验证失败");
	}
	
	// 登录
	@ApiOperation(value = "用户注册")
	@RequestMapping(value = "/reg",method = RequestMethod.POST)
	public Object reg(@ApiParam(required = true, value = "需要参数:手机号,密码,验证码") @RequestBody HoUserBean bean) {
		Assert.isNotBlank(bean.getPhone(), "手机号码为空");
		Assert.isNotBlank(bean.getPassword(), "密码为空");
		Assert.isNotBlank(bean.getCaptchacode(), "验证码为空");
		userService.reg(bean.getPhone(), bean.getPassword(),bean.getCaptchacode());
		return setSuccessModelMap("");
	}
	
	// 登录
	@ApiOperation(value = "用户登录")
	@RequestMapping(value = "/login",method = RequestMethod.POST)
	public Object login(@ApiParam(required = true, value = "需要参数:账号,密码") @CurrentUserBean HoUserBean bean) {
		Assert.isNotBlank(bean.getUsername(), "用户名为空");
		Assert.isNotBlank(bean.getPassword(), "密码为空");
		HoUser user = new HoUser();
		BeanUtils.copyProperties(bean, user, new String[] {});
		user = userService.login(user);
		String oldToken = user.getToken();
		String token = SecurityUtil.encryptMd5(SecurityUtil.encryptSHA(System.currentTimeMillis() + 
				user.getUsername()+user.getPassword()));
		user.setToken(token);
		userService.update(user);
		user.setPassword("");
		if(StringUtils.isNotEmpty(oldToken))
		{
			String key =  new StringBuilder(Constants.CACHE_NAMESPACE).
					append(Constants.CACHE_TOKEN).append(oldToken).toString();
			RedisUtil.del(key);
		}
		return setSuccessModelMap(user);
	}
	
	// 登录
	@ApiOperation(value = "用户信息")
	@GetMapping("/info")
	@Authorization
	@ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "header")
	public Object info(@CurrentUser HoUser user) {
		return setSuccessModelMap(user);
	}
	
	// 登出
	@ApiOperation(value = "用户登出")
	@GetMapping("/logout")
	@Authorization
	@ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "header")
	public Object logout(@CurrentUser HoUser user) {
		String key =  new StringBuilder(Constants.CACHE_NAMESPACE).
				append(Constants.CACHE_TOKEN).append(user.getToken()).toString();
		user.setToken("");
		userService.update(user);
		RedisUtil.del(key);
		return setSuccessModelMap("");
	}
	
//	
	// 修改密码
	@ApiOperation(value = "修改密码")
	@RequestMapping(value = "/updatepassword",method = RequestMethod.POST)
	@Authorization
	@ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "header")
	public Object updatepassword(@CurrentUser HoUser user,@ApiParam(required = true, value = "需要参数:密码") @CurrentUserBean HoUserBean bean) {
		Assert.isNotBlank(bean.getPassword(), "password参数为空");	
		userService.updatePassword(user,bean);
		return setSuccessModelMap("");
	}
	
	
	
	
//	// 修改密码
//	@ApiOperation(value = "修改密码")
//	@RequestMapping(value = "/update/password")
//	public Object updatePassword(ModelMap modelMap, @RequestParam(value = "id", required = false) Integer id,
//			@RequestParam(value = "password", required = false) String password) {
//		sysUserService.updatePassword(id, password);
//		return setSuccessModelMap(modelMap);
//	}
}
