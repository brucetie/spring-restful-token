package com.molyfun.model.student;

import com.molyfun.core.base.BaseModel;

@SuppressWarnings("serial")
public class MhStudentGroup extends BaseModel {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column mh_student_group.GROUPID
     *
     * @mbggenerated
     */
    private String groupid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column mh_student_group.STUDENTID
     *
     * @mbggenerated
     */
    private String studentid;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column mh_student_group.GROUPID
     *
     * @return the value of mh_student_group.GROUPID
     *
     * @mbggenerated
     */
    public String getGroupid() {
        return groupid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column mh_student_group.GROUPID
     *
     * @param groupid the value for mh_student_group.GROUPID
     *
     * @mbggenerated
     */
    public void setGroupid(String groupid) {
        this.groupid = groupid == null ? null : groupid.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column mh_student_group.STUDENTID
     *
     * @return the value of mh_student_group.STUDENTID
     *
     * @mbggenerated
     */
    public String getStudentid() {
        return studentid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column mh_student_group.STUDENTID
     *
     * @param studentid the value for mh_student_group.STUDENTID
     *
     * @mbggenerated
     */
    public void setStudentid(String studentid) {
        this.studentid = studentid == null ? null : studentid.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table mh_student_group
     *
     * @mbggenerated
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", groupid=").append(groupid);
        sb.append(", studentid=").append(studentid);
        sb.append("]");
        return sb.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table mh_student_group
     *
     * @mbggenerated
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        MhStudentGroup other = (MhStudentGroup) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getGroupid() == null ? other.getGroupid() == null : this.getGroupid().equals(other.getGroupid()))
            && (this.getStudentid() == null ? other.getStudentid() == null : this.getStudentid().equals(other.getStudentid()));
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table mh_student_group
     *
     * @mbggenerated
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getGroupid() == null) ? 0 : getGroupid().hashCode());
        result = prime * result + ((getStudentid() == null) ? 0 : getStudentid().hashCode());
        return result;
    }
}