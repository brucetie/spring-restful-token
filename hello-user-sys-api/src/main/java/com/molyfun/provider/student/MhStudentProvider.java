package com.molyfun.provider.student;

import com.molyfun.core.base.BaseProvider;
import com.molyfun.model.student.MhStudent;

/**

 * 创建日期：2016-7-7下午5:39:56

 * 作者：石冬生
 */
public interface MhStudentProvider extends BaseProvider<MhStudent> {
	
	
	/*public MhStudent queryUserByAccountAndPassword(MhStudent student);

	public MhStudent queryByToken(String token);
	
	public MhStudent updateToken(MhStudent record, String oldToken);*/
	
	public String findInviteCode(String cardId);
	
	public MhStudent queryByCardId(String cardId);
    
	
}
