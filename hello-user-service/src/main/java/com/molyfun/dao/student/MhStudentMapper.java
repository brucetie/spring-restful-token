package com.molyfun.dao.student;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.molyfun.core.base.BaseMapper;
import com.molyfun.model.student.MhStudent;

/**
 * 由MyBatis Generator工具自动生成，请不要手动修改
 */
public interface MhStudentMapper extends BaseMapper<MhStudent> {
        
	List<MhStudent> selectByCardId(@Param("cardId")String cardId);
    String getInviteCode(@Param("cardId")String cardId);
    void add(@Param("stu")MhStudent stu);
}