package com.molyfun.dao.student;

import com.molyfun.core.base.BaseMapper;
import com.molyfun.model.student.MhStudentCapacityEvaluation;

/**
 * 由MyBatis Generator工具自动生成，请不要手动修改
 */
public interface MhStudentCapacityEvaluationMapper extends BaseMapper<MhStudentCapacityEvaluation> {
}